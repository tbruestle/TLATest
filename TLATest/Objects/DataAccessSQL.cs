﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace TLATest.Objects
{
    public static class DataAccessSQL
    {
        #region "Connection"
        static SqlConnection conn = null;
        public static string ConnectionString
        {
            get { return ConfigurationManager.AppSettings.Get("SQLConnectionString"); }
        }
        public static SqlConnection Connection
        {
            get
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                return conn;
            }
        }
        #endregion
        #region "OneLetter"
        #region "Get Table"
        public static DataTable P_OneLetter_Get_Table()
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("P_OneLetter_Get_Table");

                cmd.Connection = Connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = 0;

                //Fill the dataset
                adapter.Fill(ds);
                Connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
        #region "TwoLetter"
        #region "Get Table"
        public static DataTable P_TwoLetter_Get_Table()
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("[dbo].[P_TwoLetter_Get_Table]");

                cmd.Connection = Connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = 0;

                //Fill the dataset
                adapter.Fill(ds);
                Connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
    }
}