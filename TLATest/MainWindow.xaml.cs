﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TLATest.Objects;

namespace TLATest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DataTable m_dt_oneletter = null;
        DataTable m_dt_twoletter = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            m_dt_oneletter = DataAccessSQL.P_OneLetter_Get_Table();
            m_dt_twoletter = DataAccessSQL.P_TwoLetter_Get_Table();

            cbFirstLetter.ItemsSource = m_dt_oneletter.DefaultView;
            cbFirstLetter.DisplayMemberPath = "value";
            cbFirstLetter.SelectedValuePath = "id";

            cbSecondLetter.ItemsSource = m_dt_oneletter.DefaultView;
            cbSecondLetter.DisplayMemberPath = "value";
            cbSecondLetter.SelectedValuePath = "id";

            cbTwoLetterAcronym.ItemsSource = m_dt_twoletter.DefaultView;
            cbTwoLetterAcronym.DisplayMemberPath = "fulltext";
            cbTwoLetterAcronym.SelectedValuePath = "id";
        }

        private void cbFirstLetter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbSecondLetter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbTwoLetterAcronym_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
